# Free Market ReactJS App

# Mobile first desing

## Gallery
![img.png](resources/gallery.png)

## Backoffice
![img.png](resources/backoffice.png)

## Home
![img.png](resources/home.png)

## Run the App in Local Machine
 Install the required dependencies
```
    yarn install
```

Start the application.
```
    yarn start
```
