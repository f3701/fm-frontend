#************************************************************
# Building
#************************************************************
FROM node:14 as build-stage

COPY . /usr/src/fm-frontend

WORKDIR /usr/src/fm-frontend

RUN yarn install

ARG REACT_APP_BACKEND_BASE_URL
ENV REACT_APP_BACKEND_BASE_URL=${REACT_APP_BACKEND_BASE_URL}

RUN npm run build

#************************************************************
# Create Image only with the builded files
#************************************************************
FROM socialengine/nginx-spa

COPY --from=build-stage /usr/src/fm-frontend/build /app
