import {BASE_URL_INVENTORY} from "./InventoryService";
import {delete_, get, post, put} from "./BackendService";
import {removeEmptyValues} from "../utils/ObjectUtil";

const CREATE_PRODUCT_STEP1_ENDPOINT = BASE_URL_INVENTORY + '/products';
const UPDATE_PRODUCT_STEP1_ENDPOINT = BASE_URL_INVENTORY + '/products';
const FETCH_PRODUCTS_ENDPOINT = BASE_URL_INVENTORY + '/products';
const FETCH_PAGE_PRODUCTS_ENDPOINT = BASE_URL_INVENTORY + '/products/page';
const DELETE_PRODUCT_ENDPOINT = BASE_URL_INVENTORY + '/products';


export const createProductStep1 = (data) => {
    return post(CREATE_PRODUCT_STEP1_ENDPOINT, data);
}

export const updateProductStep1 = (id, data) => {
    return put(UPDATE_PRODUCT_STEP1_ENDPOINT + `/${id}`, data);
}

export const fetchProducts = () => {
    return get(FETCH_PRODUCTS_ENDPOINT);
}

export const fetchProduct = (id) => {
    return get(FETCH_PRODUCTS_ENDPOINT + `/${id}`);
}

export const fetchProductsPage = (page = 0, filters = {}) => {

    const filtersAsQueryParams = new URLSearchParams(filters).toString();
    return get(FETCH_PAGE_PRODUCTS_ENDPOINT + `?page=${page}&${filtersAsQueryParams}`);

}

export const deleteProductAction = (id) => {
    return delete_(DELETE_PRODUCT_ENDPOINT + `/${id}`);
}

