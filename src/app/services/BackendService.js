import axios from 'axios';
import {BACKEND_BASE_URL} from "./BackendConstants";

const axiosInstance = axios.create({
  baseURL: BACKEND_BASE_URL
});

export const get = (endpoint) => {
  return axiosInstance.get(endpoint)
    .then(
      value => {
        return value.data.data;
      }
    )
    .catch(function (error) {
      return throwErrorMessage(error);
    })
};

export const post = (endpoint, body) => {
  return axiosInstance.post(endpoint, body)
      .then(
          value => {
            return value.data;
          }
      )
      .catch(function (error) {

        return throwErrorMessage(error);

      })
};

export const put = (endpoint, body) => {
    return axiosInstance.put(endpoint, body)
        .then(
            value => {
                return value.data;
            }
        )
        .catch(function (error) {
            return throwErrorMessage(error);
        })
};

export const delete_ = (endpoint) => {
    return axiosInstance.delete(endpoint)
        .then(
            value => {
                return value?.data;
            }
        )
        .catch(function (error) {
            return throwErrorMessage(error);
        })
};

const throwErrorMessage = (error) => {

  if (!error.response) {
    throw "Error de conexion.";
  }
  if (
    error.response.status === 401 || //unauthenticated
    error.response.status === 403 || //unauthorized
    error.response.status === 409 || //conflict
    error.response.status === 400    //bad request
  ) {
    throw error.response.data.message || error.response.data.data.message ;
  }

  // 404, 400(invalid syntax), 405(method not allowed), 5XX(server error), ...
  throw undefined;

}
