import {delete_, get, put} from "./BackendService";
import {BACKEND_BASE_URL} from "./BackendConstants";

export const BASE_URL_DRIVE = BACKEND_BASE_URL +  "/drive/api/v1";

export const ENDPOINT_FILES = BASE_URL_DRIVE + "/files";
export const ENDPOINT_NEWS_IMAGES = BASE_URL_DRIVE + "/news-images";
export const ENDPOINT_NEWS_IMAGE = BASE_URL_DRIVE + "/news-image";

export const ENDPOINT_PRODUCT_IMAGES = BASE_URL_DRIVE + "/product-images";


export const getAllNewsImages = () => {
    return get(ENDPOINT_NEWS_IMAGES);
};

export const getActiveNewsImages = () => {
    const STATUS_ACTIVE = 'ACTIVE';
    return get(ENDPOINT_NEWS_IMAGES + `?status=${STATUS_ACTIVE}`);
};

export const getArchivedNewsImages = () => {
    const STATUS_ARCHIVED = 'ARCHIVED';
    return get(ENDPOINT_NEWS_IMAGES + `?status=${STATUS_ARCHIVED}`);
};

export const deleteNewsImage = (id = '') => {
    return delete_(ENDPOINT_NEWS_IMAGE + `/${id}`);
};

export const archiveNewsImage = (id = '') => {
    return put(ENDPOINT_NEWS_IMAGE + `/${id}`, {
        'status': 'ARCHIVED'
    });
};

export const enableNewsImage = (id = '') => {
    return put(ENDPOINT_NEWS_IMAGE + `/${id}`, {
        'status': 'ACTIVE'
    });
};



