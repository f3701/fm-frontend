import styled from 'styled-components';

export const LayoutContainer = styled.div`

  display: flex;
  flex-direction: column;
  height: 100%;
  background-color: darkgray;

`;

export const LayoutMainContent = styled.div`

  overflow: auto;
  height: 100%;
  position: relative;

  display: flex;
  flex-direction: column;
  row-gap: 10px;
  column-gap: 10px;
  padding-top: 20px;
  
  align-items: center;
  
`;
