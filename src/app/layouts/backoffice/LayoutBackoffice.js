import React from 'react';
import Footer from "../../components/footer/Footer";
import SpacerBlock from "../../components/spacer-block/SpacerBlock";
import NavbarTop from "../../navbar/NavbarTop";
import {LayoutContainer, LayoutMainContent} from "./LayoutBackofficeStyles";

const LayoutBackoffice = ({children}) => {
    return (
        <LayoutContainer>

            <NavbarTop/>

            <LayoutMainContent>
                {children}
                <SpacerBlock/>
                <Footer/>
            </LayoutMainContent>

        </LayoutContainer>
    );
};

export default LayoutBackoffice;