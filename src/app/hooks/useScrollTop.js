import React, { useEffect } from 'react';

// Position scroll at the beginning of the page
const useScrollTop = (func, deps = []) => {
    useEffect(() => {
        window.scrollTo(0, 0);
    }, deps)
}

export default useScrollTop;
