import React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import LayoutBackoffice from '../layouts/backoffice/LayoutBackoffice';
import LayoutMain from "../layouts/main/LayoutMain";
import ActiveNews from '../pages/backoffice/active-news/ActiveNews';
import ArchivedNews from '../pages/backoffice/archived-news/ArchivedNews';
import ProductAddEdit from '../pages/backoffice/products/addEditProduct/ProductAddEdit';
import ProductList from '../pages/backoffice/products/productList/ProductList';
import GalleryPage from '../pages/gallery/GalleryPage';
import HomePage from '../pages/home/HomePage';


const RouterMain = () => {

    return (
        <BrowserRouter>
            <Switch>

                {/* Private */}
                <Route
                    path="/backoffice"
                    render={({ match: { url } }) => (
                        <LayoutBackoffice>
                            <Switch>
                                <Route path={`${url}/`} component={ActiveNews} exact />
                                <Route path={`${url}/active-news`} component={ActiveNews} />
                                <Route path={`${url}/archived-news`} component={ArchivedNews} />
                                <Route path={`${url}/create-products`} component={ProductAddEdit} />
                                <Route path={`${url}/edit-products/:id`} component={ProductAddEdit} />
                                <Route path={`${url}/product-list`} component={ProductList} />
                            </Switch>
                        </LayoutBackoffice>
                    )}
                />

                {/* Public */}
                <LayoutMain>
                    <Switch>
                        <Route path="/" component={HomePage} exact />
                        <Route path='/gallery' component={GalleryPage} exact />
                    </Switch>
                </LayoutMain>

            </Switch>
        </BrowserRouter>
    );
};

export default RouterMain;
