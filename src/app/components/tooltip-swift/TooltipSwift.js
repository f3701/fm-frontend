import React from 'react';
import {TooltipSwiftContainer} from "./TooltipSwiftStyles";
import {Tooltip} from "antd";

const TooltipSwift = ({text, children}) => {
    return (
        <TooltipSwiftContainer className="display-inline-block">
            <Tooltip title={text}>
                {children}
            </Tooltip>
        </TooltipSwiftContainer>
    );
};

export default TooltipSwift;