import styled from 'styled-components';

export const NavbarContainer = styled.div`

  .ant-menu-dark .ant-menu-item{
    color: rgba(255, 255, 255, 0.85);
  }
  
`;
