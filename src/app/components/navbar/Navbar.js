import React, {useEffect, useState} from 'react';
import './NavbarStyles';
import {useHistory, useLocation} from 'react-router-dom';
import {Header} from 'antd/es/layout/layout';
import {Menu} from 'antd';
import {NavbarContainer} from "./NavbarStyles";

const Navbar = () => {

    const history = useHistory();

    const location = useLocation();
    const {pathname} = location;

    const [selectedKeys, setSelectedKeys] = useState(["/"]);

    const keysMenu = {
        home: "/",
        gallery: "/gallery",
        active_news: "/backoffice/active-news",
        archived_news: "/backoffice/archived-news"
    };

    useEffect(() => {
        if (Object.values(keysMenu).includes(pathname)) {
            setSelectedKeys([pathname])
        }
    }, [pathname])

    return (
        <NavbarContainer>
            <Header>
                <Menu
                    theme='dark'
                    mode='horizontal'
                    selectable={true}
                    selectedKeys={selectedKeys}
                >
                    <Menu.Item
                        key={keysMenu.home}
                        onClick={() => history.push(keysMenu.home)}
                    >
                        Home
                    </Menu.Item>

                    <Menu.Item
                        key={keysMenu.gallery}
                        onClick={() => history.push(keysMenu.gallery)}
                    >
                        Gallery
                    </Menu.Item>
                    <Menu.Item
                        key={keysMenu.active_news}
                        onClick={() => history.push(keysMenu.active_news)}
                    >
                        Active News
                    </Menu.Item>
                    <Menu.Item
                        key={keysMenu.archived_news}
                        onClick={() => history.push(keysMenu.archived_news)}
                    >
                        Archived News
                    </Menu.Item>
                </Menu>
            </Header>
        </NavbarContainer>
    );
};

export default Navbar;
