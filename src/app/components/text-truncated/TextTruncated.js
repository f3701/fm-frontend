import React from 'react';
import {TextTruncatedContainer} from "./TextTruncatedStyles";
import LinesEllipsis from "react-lines-ellipsis";

const TextTruncated = ({lines, text}) => {
    return (
        <TextTruncatedContainer>
            <LinesEllipsis
                text={text}
                maxLine={lines}
                ellipsis=' ... '
                trimRight
                basedOn='letters'
            />
        </TextTruncatedContainer>
    );
};

export default TextTruncated;