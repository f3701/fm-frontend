import styled from 'styled-components';

export const Container = styled.div`
  
  padding: 15px 5px;
  

  @media screen and (min-width: 576px) {
    padding: 45px 10%;
  }
  
`;

