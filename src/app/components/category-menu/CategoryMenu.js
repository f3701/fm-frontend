import React from 'react';
import {UnorderedListOutlined} from "@ant-design/icons";
import {Tooltip} from "antd";
import {CategoryMenuContainer} from "./CategoryMenuStyles";

const CategoryMenu = () => {

    return (
        <CategoryMenuContainer>
            <Tooltip title="Categories soon...">
                <UnorderedListOutlined
                    className= 'icon-md'
                />
            </Tooltip>
        </CategoryMenuContainer>

    );
};

export default CategoryMenu;
