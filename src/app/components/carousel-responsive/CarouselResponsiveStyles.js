import styled from 'styled-components';

export const CarouselResponsiveContainer = styled.div`
  margin: 20px 0;
  position: relative;

  .slider {
    display: flex;
    align-items: center;
  }
`;

