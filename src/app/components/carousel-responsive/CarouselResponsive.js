import React from 'react';
import {Carousel} from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {CarouselResponsiveContainer} from "./CarouselResponsiveStyles";

const CarouselResponsive = ({images = [], maxHeight, children}) => {

    return (
        <CarouselResponsiveContainer>
            {children}
            <Carousel
                showArrows={true}
                autoPlay={true}
                infiniteLoop={true}
                showThumbs={false}
            >
                {images.map(image =>
                    <img
                        src={image.url}
                        alt="image"
                        key={image.id}
                        style={{maxHeight: maxHeight}}
                    />
                )}
            </Carousel>
        </CarouselResponsiveContainer>
    );
};

export default CarouselResponsive;
