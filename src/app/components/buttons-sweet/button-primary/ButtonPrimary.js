import React from 'react';
import {Button} from "react-bootstrap";
import {ButtonPrimaryContainer} from "./ButtonPrimaryStyles";

const ButtonPrimary = ({
                           title = '',
                           onClick = () => console.log('on click event.'),
                           type = ''
                       }) => {
    return (
        <ButtonPrimaryContainer>
            <Button
                variant="primary"
                type={type}
                onClick={onClick}
            >
                {title}
            </Button>
        </ButtonPrimaryContainer>
    );
};

export default ButtonPrimary;