import React from 'react';
import {Button} from "react-bootstrap";
import {ButtonDangerContainer} from "./ButtonDangerStyles";

const ButtonDanger = ({
                          title = '',
                          onClick = () => console.log('on click event.'),
                          type = ''
                      }) => {
    return (
        <ButtonDangerContainer>
            <Button
                variant="danger"
                onClick={onClick}
                type={type}
            >
                {title}
            </Button>
        </ButtonDangerContainer>
    );
};

export default ButtonDanger;