import React from 'react';
import {Button} from "react-bootstrap";
import {ButtonAlternativeContainer} from "./ButtonAlternativeStyles";

const ButtonAlternative = ({
                          title = '',
                          onClick = () => console.log('on click event.'),
                          type = ''
                      }) => {
    return (
        <ButtonAlternativeContainer>
            <Button
                variant="secondary"
                onClick={onClick}
                type={type}
            >
                {title}
            </Button>
        </ButtonAlternativeContainer>
    );
};

export default ButtonAlternative;