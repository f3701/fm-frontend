import React from 'react';
import {ErrorListContainer} from "./ErrorListStyles";

const ErrorList = ({ errors = [] }) => {
    return (
        <ErrorListContainer>
            <ul>
                {errors.map(error => <li key={error}> {error} </li>)}
            </ul>
        </ErrorListContainer>
    );
};

export default ErrorList;