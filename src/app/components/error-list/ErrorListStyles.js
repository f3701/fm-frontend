import styled from 'styled-components';

export const ErrorListContainer = styled.div`
  li {
    color: orangered;
    font-size: 1.25em;
    font-weight: bold;
  }
`;