import React from 'react';
import {SpacerBlockContainer} from "./SpacerBLockStyles";

const SpacerBlock = () => {
    return (
        <SpacerBlockContainer/>
    )
};

export default SpacerBlock;
