import styled from 'styled-components';

export const SpacerBlockContainer = styled.div`
    flex-grow: 2;
`;
