import React from 'react';
import {Copyright, FooterContainer} from "./FooterStyles";

const Footer = () => {
    return (
        <FooterContainer>
            <Copyright>©2022-2023 DLabs SRL. All Rights Reserved.</Copyright>
        </FooterContainer>
)
};

export default Footer;
