import styled from 'styled-components';

export const FooterContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  //background-color: #001529;
  padding: 15px 0;
`;

export const Copyright = styled.div`
  color: #001529;
  font-size: 1.2em;
`;