import {Pagination} from 'antd';
import React from 'react';
import "./PaginatorStyles.js";
import {Container} from './PaginatorStyles.js';

function Paginator(
    {
        show = true,
        changePageCallback = (pageNumber) => {},
        page
    }
) {

    const {number = 0, totalElements = 0, size = 10} = page;

    return (
        <Container>
            {show &&
                <Pagination
                    current={number + 1}
                    total={totalElements}
                    onChange={changePageCallback}
                    pageSize={size}
                    size="large"
                    locale={{items_per_page: ' / Página'}}
                    showLessItems={true}
                />
            }
        </Container>
    )
}

export default Paginator
