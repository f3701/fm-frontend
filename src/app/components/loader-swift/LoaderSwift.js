import React from 'react';
import {BarLoader} from "react-spinners";
import {LoaderSwiftContainer} from "./LoaderSwiftStyles";
import {css} from "@emotion/react";

/**
 * Show the loader in the closest parent container that has relative positioning
 * Size: 100% wight and 100% height of parent.
 * @param show
 * @param color
 * @returns {JSX.Element}
 * @constructor
 */
const LoaderSwift = ({
                         show = true,
                         color = '#EFEFEF'
                     }) => {
    // Can be a string as well. Need to ensure each key-value pair ends with ;
    const override = css`
      width: 100%
    `;
    return (
        <LoaderSwiftContainer>
            <BarLoader
                color={color}
                loading={show}
                height={6}
                css={override}
                speedMultiplier={2}
            />
        </LoaderSwiftContainer>
    );
};

export default LoaderSwift;