import styled from 'styled-components';

export const LoaderSwiftContainer = styled.div`
  position: absolute;
  z-index: 999;
  background-color: rgba(0, 0, 0, 0.25);
  width:100%;
  height:100%;
  display: flex;
  justify-content: center;
  align-items: baseline;
`;

