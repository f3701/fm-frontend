import swal from "sweetalert";

export const alertUnimplemented = () => {
    swal(
        'Proximamente...',
        '',
        'warning'

    )
}

export const confirmSweet = (message = "", details = "") => {
    return swal({
        title: message,
        text: details,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    });
}
