import React, {useEffect, useState} from 'react';
import {FilterTextContainer} from "./FilterTextStyles";

const FilterText = (
    {
        submitCallback = (value) => console.log('Submitting value: ' + value),
        placeholder = 'Placeholder',
        defaultValue = ''
    }
) => {

    const [textFilter, setTextFilter,] = useState('');

    const handleInputChange = (event) => {
        setTextFilter(event.target.value);
    }

    const handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            submitCallback(event.target.value);
        }
    }

    useEffect(() => {
        if (defaultValue || defaultValue === ""){
            setTextFilter(defaultValue);
        }
    }, [defaultValue]);

    return (
        <FilterTextContainer>
            <input
                type="text"
                placeholder={placeholder}
                onKeyPress={handleKeyPress}
                value={textFilter}
                onChange={handleInputChange}
                className='inputTextFilter'
            />
        </FilterTextContainer>
    );
};

export default FilterText;