import styled from 'styled-components';

export const FilterTextContainer = styled.div`
  .inputTextFilter {
    padding: 0 5px;
  }
  input {
    width: 150px;
  }
`;