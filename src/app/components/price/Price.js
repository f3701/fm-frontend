import React from 'react';
import {PriceContainer} from "./PriceStyles";

const Price = ({value}) => {
    return (
        <PriceContainer>
            ${value}
        </PriceContainer>
    );
};

export default Price;