import styled from 'styled-components';

export const PriceContainer = styled.div`
  font-size: 1.5em;
  font-weight: bolder;
`;
