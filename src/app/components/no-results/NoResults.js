import React from 'react';
import {NoResultsContainer} from "./NoResultsStyles";

const NoResults = () => {
    return (
        <NoResultsContainer>
            No Results...
        </NoResultsContainer>
    );
};

export default NoResults;