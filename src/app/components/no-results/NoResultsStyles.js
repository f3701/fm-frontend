import styled from 'styled-components';

export const NoResultsContainer = styled.div`
  text-align: center;
  font-size: 1.5em;
  font-weight: lighter;
`;
