import React from 'react';
import {Card, Col, Row} from "antd";
import LinesEllipsis from 'react-lines-ellipsis'
import {Container, ImageContainer, TextContainer} from './CardAntdStyles';
import {ENDPOINT_FILES} from "../../services/BackendConstants";

const CardAntd = ({property}) => {

    const {fileId, title, address, surface, description} = property;
    return (
        <Container>
            <Card>
                <Row>
                    <Col xs={24}  xl={8}>
                        <ImageContainer>
                            <img src={`${ENDPOINT_FILES}/${fileId}`} alt=""/>
                        </ImageContainer>
                    </Col>
                    <Col xs={24}  xl={16}>
                        <TextContainer>
                            <div id="title"><span id="propertyStatus"></span>{title}</div>
                            <div id="address">{address}</div>
                            <div id="surface"><span>{surface}</span></div>
                            <div id="description">
                                {description &&
                                <LinesEllipsis
                                    text={description}
                                    maxLine='3'
                                    ellipsis=' ... '
                                    trimRight
                                    basedOn='letters'
                                />
                                }
                            </div>
                        </TextContainer>
                    </Col>
                </Row>
            </Card>
        </Container>
    );
};

export default CardAntd;
