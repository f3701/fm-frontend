import React from 'react';

const Title = ({value}) => {
    return (
        <h5>{value}</h5>
    );
};

export default Title;