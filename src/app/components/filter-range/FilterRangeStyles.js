import styled from 'styled-components';

export const FilterRangesContainer = styled.div`
  display: flex;
  column-gap: 5px;
  input {
    width: 75px;
  }
`;