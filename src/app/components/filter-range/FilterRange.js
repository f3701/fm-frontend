import React, {useEffect} from 'react';
import {FilterRangesContainer} from "./FilterRangeStyles";
import {useForm} from "../../hooks/useForm";

const FilterRange = (
    {
        submitCallback = (value) => console.log('Submitting value: ' + value),
        filterName = '',
        minDefaultValue = '',
        maxDefaultValue = '',
    }
) => {

    const initialValues = {
        minValue: '',
        maxValue: ''
    }
    const [form, handleInputChange, _, setForm] = useForm(initialValues)
    const {minValue, maxValue} = form;

    const handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            submitCallback({
                minValue,
                maxValue
            });
        }
    }

    useEffect(() => {
        setForm({
            ...form,
            minValue: minDefaultValue,
            maxValue: maxDefaultValue
        });
    }, [minDefaultValue, maxDefaultValue]);

    return (
        <FilterRangesContainer>
            <input
                type="text"
                placeholder={`Min ${filterName}`}
                onKeyPress={handleKeyPress}
                name="minValue"
                value={minValue}
                onChange={handleInputChange}
            />
            <input
                type="text"
                placeholder={`Max ${filterName}`}
                onKeyPress={handleKeyPress}
                name="maxValue"
                value={maxValue}
                onChange={handleInputChange}
            />
        </FilterRangesContainer>
    );
};

export default FilterRange;