import React from 'react';
import {SearchContainer} from './SearchFStyles';
import Search from "antd/es/input/Search";

const SearchBar = ({
                       callbackSearch = (value) => {
                           console.log(value);
                       },
                       defaultSearchValue = '',
                       placeholder = 'Search products...'
                   }) => {

    const handlerSearch = (text) => {
        callbackSearch(text);
    }

    return (
        <SearchContainer>
            <div id="searchBox">
                <Search
                    placeholder={placeholder}
                    loading={false}
                    size={'large'}
                    enterButton={true}
                    onSearch={handlerSearch}
                    defaultValue={defaultSearchValue}
                />
            </div>
        </SearchContainer>

    );
};

export default SearchBar;
