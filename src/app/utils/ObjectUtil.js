/**
 * Removes undefined, null and ""
 * @param obj
 * @returns {*}
 */
export const removeEmptyValues = (obj) => {
    for (let key in obj) { /* You can get copy by spreading {...query} */
        if (obj[key] === undefined /* In case of undefined assignment */
            || obj[key] === null
            || obj[key] === ""
        ) {
            delete obj[key];
        }
    }
    return obj;
}
