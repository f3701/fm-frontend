import styled from 'styled-components';

export const NavbarTopContainer = styled.div`
  
  .nav-link {
    font-size: 16px !important;
    color: #EDEDED !important;
  }
 
`;
