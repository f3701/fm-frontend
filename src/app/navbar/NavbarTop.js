import React, {useEffect, useState} from 'react';
import './NavbarTopStyles';
import {Link, useLocation} from 'react-router-dom';
import {Container, Modal, Nav, Navbar, NavDropdown} from "react-bootstrap";
import {NavbarTopContainer} from "./NavbarTopStyles";
import Login from "../security/login/Login";

const NavbarTop = () => {

    const location = useLocation();
    const {pathname} = location;


    const keysMenu = {
        home: "/",
        gallery: "/gallery",
        active_news: "/backoffice/active-news",
        archived_news: "/backoffice/archived-news",
        create_products: "/backoffice/create-products",
        product_list: "/backoffice/product-list",
    };

    useEffect(() => {
    }, [pathname])

    const [showLogin, setShowLogin] = useState(false);
    const loginModal = (
        <Modal
            show={showLogin}
            onHide={() => setShowLogin(false)}
        >
            <Modal.Header closeButton/>
            <Modal.Body>
                <Login/>
            </Modal.Body>
            <Modal.Footer/>
        </Modal>
    );

    return (
        <NavbarTopContainer>
            <Navbar bg="dark" variant="dark" expand="sm">
                <Container>
                    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link as={Link} to={keysMenu.home}> Home </Nav.Link>
                            <Nav.Link as={Link} to={keysMenu.gallery}> Gallery </Nav.Link>
                            <NavDropdown title="Backoffice" id="basic-nav-dropdown">
                                <NavDropdown.Item as={Link} to={keysMenu.active_news}>Active News</NavDropdown.Item>
                                <NavDropdown.Item as={Link} to={keysMenu.archived_news}>Archived News</NavDropdown.Item>
                                <NavDropdown.Item as={Link} to={keysMenu.create_products}>Create
                                    Products</NavDropdown.Item>
                                <NavDropdown.Item as={Link} to={keysMenu.product_list}>Product List</NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                    <Nav.Link
                        onClick={() => setShowLogin(true)}
                    >
                        Login
                    </Nav.Link>
                </Container>
            </Navbar>
            {loginModal}
        </NavbarTopContainer>
    );
};

export default NavbarTop;
