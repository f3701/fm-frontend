import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';
import {shoppingReducer} from "./shopping/reducer";

const appReducer = combineReducers({
    shopping: shoppingReducer,
});

const initialState = {};

const rootReducer = (state, action) => {
    return appReducer(state, action);
};

const middleware = [thunk];

const store = createStore(rootReducer, initialState, composeWithDevTools(applyMiddleware(...middleware)));

export default store;
