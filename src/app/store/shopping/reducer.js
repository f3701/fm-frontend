import {SET_TARGET_SEARCH, UNSET_TARGET_SEARCH} from "./actionTypes";

export const defaultState = {
    targetSearch: ''
}
export const shoppingReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SET_TARGET_SEARCH:
            return {...state, targetSearch: action.payload};
        case UNSET_TARGET_SEARCH:
            return {...state, targetSearch: ''};
        default:
            return state;
    }
};
