import React from 'react';
import {Button, Form, Input} from 'antd';
import {LoginContainer} from "./LoginStyles";
import {UserOutlined} from "@ant-design/icons";

const Login = () => {

    const onFinish = (values) => {
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <LoginContainer>
            <UserOutlined className="login-icon"/>
            <Form
                name="basic"
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    label="Username"
                    name="username"
                    rules={[{required: true, message: 'Username is required.'}]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[{required: true, message: 'Password is required.'}]}
                >
                    <Input.Password/>
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit" size="lg">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </LoginContainer>
    );
};

export default Login;