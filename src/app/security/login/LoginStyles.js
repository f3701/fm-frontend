import styled from 'styled-components';

export const LoginContainer = styled.div`


  background-color: white;
  padding: 20px 10px 0;
  //max-width: 420px;

  .login-icon {
    font-size: 32px;
    display:block;
  }
  
  button {
    width: 100%;
  }

  label {
    font-size: 18px;
  }

  input {
    font-size: 16px;
  }

  button[type='submit'] {
    font-weight: 500;
    margin-top: 5px;
  }

  .ant-row {
    display: block !important;
  }

`;

