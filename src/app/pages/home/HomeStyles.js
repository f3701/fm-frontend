import styled from 'styled-components';
import {CarouselResponsiveContainer} from "../../components/carousel-responsive/CarouselResponsiveStyles";

export const HomeContainer = styled.div`

  ${CarouselResponsiveContainer} {
    margin: 20px 0;
  }
  
  #promotionContainer {
    margin: 15px 0;
  }
  
`;

