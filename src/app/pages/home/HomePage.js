import React, {useEffect, useState} from 'react';

import CarouselResponsive from "../../components/carousel-responsive/CarouselResponsive";
import {HomeContainer} from "./HomeStyles";
import {ENDPOINT_NEWS_IMAGE, getActiveNewsImages} from "../../services/DriveService";
import SearchBar from "../../components/search/SearchBar";
import LoaderSwift from "../../components/loader-swift/LoaderSwift";
import {toastError} from "../../components/alerts/SweetToasts";
import {useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {SET_TARGET_SEARCH} from "../../store/shopping/actionTypes";

const HomePage = () => {

    const [images, setImages] = useState([]);
    const [isLoadingNews, setIsLoadingNews] = useState(false);

    const history = useHistory();
    const dispatch = useDispatch();
    const {targetSearch} = useSelector((state) => state.shopping)

    const MAX_HEIGHT_CAROUSEL_IMAGES = 380;

    const searchProductHandler = (value) => {
        const action = {type: SET_TARGET_SEARCH, payload: value};
        dispatch(action);

        history.push("/gallery")
    }

    useEffect(() => {
        setIsLoadingNews(true);
        getActiveNewsImages()
            .then(
                images => {
                    const imagesMapped = images.map((image) => ({
                        id: image.id,
                        url: ENDPOINT_NEWS_IMAGE + `/${image.id}/file`
                    }))
                    setImages(imagesMapped)
                })
            .catch(_ => toastError("Could not load home."))
            .finally(() => setIsLoadingNews(false));
    }, []);

    return (
        <HomeContainer>

            <SearchBar
                callbackSearch={searchProductHandler}
                defaultSearchValue={targetSearch}
            />
            <CarouselResponsive
                images={images}
                maxHeight={MAX_HEIGHT_CAROUSEL_IMAGES}
            >
                {isLoadingNews && <LoaderSwift/>}
            </CarouselResponsive>

        </HomeContainer>

    );
};

export default HomePage;
