import React, {useEffect} from 'react';
import {PriceFilterContainer} from "./PriceFilterStyles";
import {Button, Form, InputNumber} from "antd";
import Title from "../../../../components/title/Title";

const PriceFilter = ({
                         onApplyFilterCallback = ({ minPrice, maxPrice }) => console.log(minPrice, maxPrice),
                         initialValue = {minPrice: '', maxPrice: ''}
                     }) => {

    const [form] = Form.useForm();

    const handleFinish = (formValues) => {
        onApplyFilterCallback({
            minPrice: formValues.minPrice || '',
            maxPrice: formValues.maxPrice || ''
        });
    }

    useEffect(() => {
        initialValue && form.setFieldsValue(initialValue)
    }, [initialValue])

    return (
        <PriceFilterContainer>
            <Title value="Price"/>
            <Form
                name="basic"
                onFinish={handleFinish}
                autoComplete="off"
                form={form}
                className="display-flex column-gap-10"
            >
                <Form.Item name="minPrice">
                    <InputNumber
                        addonBefore="$"
                        addonAfter="Min"
                    />
                </Form.Item>
                <Form.Item name="maxPrice">
                    <InputNumber
                        addonBefore="$"
                        addonAfter="Max"
                    />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        Apply
                    </Button>
                </Form.Item>
            </Form>
        </PriceFilterContainer>
    );
};

export default PriceFilter;