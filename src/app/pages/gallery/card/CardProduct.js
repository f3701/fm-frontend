import * as React from 'react';
import {CardProductContainer} from "./CardProductStyles";
import {Button, Card} from "react-bootstrap";
import Price from "../../../components/price/Price";
import LinesEllipsis from "react-lines-ellipsis";
import TextTruncated from "../../../components/text-truncated/TextTruncated";

function CardProduct({product}) {

    return (
        <CardProductContainer>
            <Card>
                <div className="img-cont">
                    <Card.Img
                        variant="top"
                        src={product.url}
                    />
                </div>
                <Card.Body>
                    <Card.Title>
                        {product.title &&
                            <TextTruncated
                                text={product.title}
                                lines={2}
                            />
                        }
                    </Card.Title>
                    <div>
                        {product.description &&
                            <TextTruncated
                                text={product.description}
                                lines={4}
                            />
                        }
                    </div>
                    <Card.Footer className="product-footer">
                        <Price value={product.price}/>
                        <Button variant="primary"
                                disabled={true}
                        >
                            Details <div className="coming-soon-label">Coming soon</div>
                        </Button>
                    </Card.Footer>
                </Card.Body>
            </Card>
        </CardProductContainer>

    );

}

export default CardProduct;