import styled from 'styled-components';

export const CardProductContainer = styled.div`
  
  max-width: 365px;
  
  .img-cont {
    width: 320px;
    height: 220px;

    display: flex;
    justify-content: center;
    align-items: center;
    
    align-self: center;
  }
  
  .img-cont img {
    object-fit: cover;
  }
  
  .product-footer {
    display:flex;
    justify-content: space-between;
  }
  
  .coming-soon-label {
    font-size: 0.75em;
  }
`;

