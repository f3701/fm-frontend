import React, {useEffect, useState} from 'react';
import {FilterListOutlined, SortByAlphaOutlined} from "@mui/icons-material";
import {GalleryContainer} from "./GalleryStyles";
import CardProduct from "./card/CardProduct";
import {ENDPOINT_PRODUCT_IMAGES} from "../../services/DriveService";
import {fetchProductsPage} from "../../services/ProductService";
import SearchBar from "../../components/search/SearchBar";
import {toastError} from "../../components/alerts/SweetToasts";
import Paginator from "../../components/paginator/Paginator";
import NoResults from "../../components/no-results/NoResults";
import {Button, Dropdown, Menu} from "antd";
import {Modal} from "react-bootstrap";
import PriceFilter from "./filters/price-filter/PriceFilter";
import LoaderSwift from "../../components/loader-swift/LoaderSwift";
import {useDispatch, useSelector} from "react-redux";
import {SET_TARGET_SEARCH} from "../../store/shopping/actionTypes";

const GalleryPage = () => {

    const initialFilters = {
        searchText: '',
        sort: 'title,asc',
        minPrice: '',
        maxPrice: ''
    }
    const [products, setProducts] = useState([]);
    const [page, setPage] = useState({});
    const [filters, setFilters] = useState(initialFilters);
    const [showLoader, setShowLoader] = useState(false);

    const {targetSearch} = useSelector((state) => state.shopping)
    const dispatch = useDispatch();

    const SORTING_CRITERIA = {
        relevance: 'title,asc',
        lowestPrice: 'price,asc',
        highestPrice: 'price,desc'
    }

    const handleSortingChange = (e) => {
        const CRITERIA_SELECTED = e.key;
        searchProducts(1, {...filters, sort: CRITERIA_SELECTED});
    }

    const sortingMenu = (
        <Menu onClick={handleSortingChange}>
            <Menu.Item key={SORTING_CRITERIA.relevance}>Relevance</Menu.Item>
            <Menu.Item key={SORTING_CRITERIA.lowestPrice}>Lowest Price</Menu.Item>
            <Menu.Item key={SORTING_CRITERIA.highestPrice}>Highest Price</Menu.Item>
        </Menu>
    );

    const handlePriceFilterChange = ({minPrice, maxPrice}) => {
        searchProducts(1, {...filters, minPrice, maxPrice});
        handleCloseFilterModal();
    }

    /**
     * Reset price filter at modal.
     */
    const handleResetModalFilters = () => {
        searchProducts(1, {
            ...filters,
            minPrice: '',
            maxPrice: ''
        });
        handleCloseFilterModal();
    }

    // Todo: Extract at separate component (ProductModal)
    const [showFilterModal, setShowFilterModal] = useState(false);
    const handleCloseFilterModal = () => setShowFilterModal(false);
    const handleShowFilterModal = () => setShowFilterModal(true);
    const filterModal = (
        <Modal show={showFilterModal} onHide={handleCloseFilterModal}>
            <Modal.Header closeButton>
                <Modal.Title>Filters</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <PriceFilter
                    onApplyFilterCallback={handlePriceFilterChange}
                    initialValue={{
                        minPrice: filters.minPrice,
                        maxPrice: filters.maxPrice
                    }}
                />
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={handleResetModalFilters}>Reset</Button>
            </Modal.Footer>
        </Modal>
    );

    const searchProducts = (page = 1, filters) => {
        setShowLoader(true);
        fetchProductsPage(page - 1, filters)
            .then(
                results => {
                    const productsWithURL = results.content.map(p => ({
                        ...p,
                        url: `${ENDPOINT_PRODUCT_IMAGES}/${p.id}/0/file`,
                    }));

                    setProducts(productsWithURL);
                    setPage({
                        number: results.number,
                        totalElements: results.totalElements,
                        size: results.size
                    });
                    setFilters(filters);
                    dispatch({type: SET_TARGET_SEARCH, payload: filters.searchText});
                }
            )
            .catch(_ => toastError('Could not load products.'))
            .finally(() => setShowLoader(false))
    }

    const changePageHandler = (pageNumber) => {
        searchProducts(pageNumber, filters)
    }

    const searchBarHandler = (valueSearched) => {
        searchProducts(1, {searchText: valueSearched});
    }

    useEffect(() => {
        if (!targetSearch) {
            searchProducts(1, filters);
        } else {
            searchProducts(1,
                {
                    ...filters,
                    searchText: targetSearch
                });
        }

    }, []);

    return (
        <GalleryContainer>
            <SearchBar
                callbackSearch={searchBarHandler}
                defaultSearchValue={targetSearch}
            />

            <div className='filters position-relative'>
                <Button onClick={handleShowFilterModal}>
                    <FilterListOutlined className="icon-md"/>
                </Button>
                {filterModal}

                <Dropdown overlay={sortingMenu}>
                    <Button>
                        <SortByAlphaOutlined
                            className="icon-md"
                        />
                    </Button>
                </Dropdown>
            </div>

            <div className='product-cont'>
                {showLoader && (<LoaderSwift/>)}
                {products?.map(product =>
                    <CardProduct key={product.id} product={product}/>
                )}
            </div>

            <Paginator
                show={products.length > 0}
                page={page}
                changePageCallback={changePageHandler}
            />
            {products.length === 0 && <NoResults/>}


        </GalleryContainer>
    );
};

export default GalleryPage;
