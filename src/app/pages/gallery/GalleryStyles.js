import styled from 'styled-components';

export const GalleryContainer = styled.div`
  
  display: flex;
  flex-direction: column;
  row-gap: 20px;
  column-gap: 20px;
  
  .product-cont {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    row-gap: 10px;
    column-gap: 10px;
    position: relative;
  }
  
  .filters > button{
    margin-right: 10px;
  }

  @media screen and (min-width: 835px) {
    .product-cont {
      justify-content: flex-start;
    }
  }
  
`;

