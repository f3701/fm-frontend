import styled from 'styled-components';

export const ProductImageContainer = styled.div`
  
    .preview-image-cont img {
      height: 200px;
    }
  
    .list-images-cont {
      display: flex;
      flex-wrap: wrap;
      row-gap: 15px;
      column-gap: 15px;
    }
  
    .file-upload-cont {
      margin: 10px 0;
    }
  
    .preview-cont {
      margin: 10px 0;
    } 

`;

