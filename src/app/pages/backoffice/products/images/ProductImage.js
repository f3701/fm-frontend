import React, {useEffect, useRef, useState} from 'react';
import {toastError, toastSuccess} from "../../../../components/alerts/SweetToasts";
import axios from "axios";
import {BASE_URL_DRIVE} from "../../../../services/DriveService";
import {ProductImageContainer} from "./ProductImageStyles";
import {delete_, get} from "../../../../services/BackendService";
import ButtonPrimary from "../../../../components/buttons-sweet/button-primary/ButtonPrimary";
import ButtonAlternative from "../../../../components/buttons-sweet/button-alternative/ButtonAlternative";
import ButtonDanger from "../../../../components/buttons-sweet/button-danger/ButtonDanger";
import {confirmSweet} from "../../../../components/alerts/SweetAlerts";

const ProductImage = ({productId}) => {

    const [images, setImages] = useState([]);
    const MAX_HEIGHT_IMAGES = 200;

    const [currentFile, setCurrentFile] = useState('');
    const [currentFileContent, setCurrentFileContent] = useState('');
    const fileInputRef = useRef();

    const handleInputFileChange = (event) => {
        event.target.files[0] && setCurrentFile(URL.createObjectURL(event.target.files[0]));
        event.target.files[0] && setCurrentFileContent(event.target.files[0]);
    }

    const handleConfirmUpload = async () => {
        await uploadFile();

        setCurrentFile('');
        fileInputRef.current.value = '';
        toastSuccess('Uploaded.')
    }

    const handleCancelUpload = () => {
        setCurrentFile('');
        fileInputRef.current.value = '';
    }

    const handleDeleteFile = async (id) => {
        if (await confirmSweet('Are you sure to DELETE this item?')) {
            await delete_(BASE_URL_DRIVE + `/product-images/${id}`);
            setImages(await fetchImages());
            toastSuccess('Deleted.');
        }
    }

    const uploadFile = async () => {
        const formData = new FormData();
        formData.append('file', currentFileContent);
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        await axios.post(BASE_URL_DRIVE + '/product-images/' + productId, formData, config)
            .catch(error => {
                toastError(error?.response?.data?.data.message);
                throw error;
            });

        setImages(await fetchImages());
    }

    const fetchImages = async () => {
        const images = (await get(BASE_URL_DRIVE + `/product-images/${productId}/images`)).reverse();
        return images.map((image) => ({
            id: image.id,
            url: BASE_URL_DRIVE + `/product-images/${image.id}/file`,
            order: image.order
        }));
    }

    useEffect(async () => {
        setImages(await fetchImages())
    }, []);

    return (
        <ProductImageContainer>

            <h1>Images</h1>

            <div className="file-upload-cont">
                <input
                    type="file"
                    name="inputUploadImage"
                    onChange={handleInputFileChange}
                    ref={fileInputRef}
                />
            </div>
            {
                currentFile && (
                    <div className='preview-cont'>
                        <div className="preview-image-cont">
                            <img src={currentFile} alt='preview image'/>
                        </div>
                        <ButtonPrimary
                            title="Upload"
                            onClick={handleConfirmUpload}
                        />
                        <ButtonAlternative
                            title="Cancel"
                            onClick={handleCancelUpload}
                        />
                    </div>
                )
            }
            <br/>
            <div className="list-images-cont">
                {
                    images.map(i => (
                        <div key={i.id}>
                            <img src={i.url} alt="img" height={MAX_HEIGHT_IMAGES}/>
                            <div>
                                <ButtonDanger
                                    title="Delete"
                                    onClick={() => handleDeleteFile(i.id)}
                                />
                                <span> Order {i.order}</span>
                            </div>
                        </div>
                    ))
                }
            </div>
        </ProductImageContainer>
    );
};

export default ProductImage;