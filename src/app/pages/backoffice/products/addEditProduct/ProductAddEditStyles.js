import styled from 'styled-components';

export const ProductAddEditContainer = styled.div`

  width: 97%;

  @media only screen and (min-width: 800px) {
    width: 800px;
  }

  #titleAddEditProduct {
    text-align: center;
  }

  .action-buttons-products {
    display: flex;
    gap: 5px;
    justify-content: flex-end;
  }

`;

