import React, {useEffect, useState} from 'react';
import {Button, Form} from "react-bootstrap";
import {useForm} from "../../../../hooks/useForm";
import {createProductStep1, fetchProduct, updateProductStep1} from "../../../../services/ProductService";
import ProductImage from "../images/ProductImage";
import {ProductAddEditContainer} from "./ProductAddEditStyles";
import {toastError, toastSuccess} from "../../../../components/alerts/SweetToasts";
import {useHistory, useParams} from 'react-router-dom';
import ErrorList from "../../../../components/error-list/ErrorList";

const ProductAddEdit = (isCreate = true) => {

    const {id} = useParams();
    const history = useHistory();

    const [titleForm, setTitleForm] = useState(!id ? 'New Product' : 'Edit Product');
    const [titleButtonConfirm, setTitleButtonConfirm] = useState(!id ? 'Create' : 'Update');

    const initialForm = {
        title: '',
        description: '',
        price: ''
    }
    const [form, handleInputChange, , setForm] = useForm(initialForm)
    const {title, description, price} = form;

    const [productId, setProductId] = useState(id ? id : '');

    const [errorList, setErrorList] = useState([]);

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            validateForm();
            if (!productId) {

                const result = await createProductStep1(form);
                setProductId(result.data.id);
                changeToUpdateTitles();
                toastSuccess('Created.')
            } else {
                await updateProductStep1(productId, form);
                toastSuccess('Updated.')
            }
        } catch (error) {
            toastError(error);
        }

    }

    const changeToUpdateTitles = () => {
        setTitleForm('Edit Product');
        setTitleButtonConfirm('Update');
    }

    // Fetch product details
    useEffect(async () => {
        if (productId) {
            const response = await fetchProduct(productId);
            setForm({
                ...response,
                price: (response.price || response.price === 0) ? response.price : ''
            });
        }

    }, [])

    const validateForm = () => {
        let errors = [];
        if (!title || title.trim().length === 0) {
            errors.push("Title is required.")
        }
        if (price === "" || price < 0) {
            errors.push("Price must be a positive value, for example: 99.99" )
        }

        setErrorList(errors);
        if (errors.length > 0) {
            throw 'The form contains validation errors.'
        }
    }


    return (
        <ProductAddEditContainer>
            <h1 id='titleAddEditProduct'>{titleForm}</h1>

            {errorList.length > 0 && <ErrorList errors={errorList}/>}

            <Form onSubmit={handleSubmit}>

                <Form.Group className="mb-3">
                    <Form.Label>Title</Form.Label>
                    <Form.Control
                        as="textarea"
                        rows={2}
                        placeholder="Product title..."
                        name='title'
                        value={title}
                        onChange={handleInputChange}
                    />
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                        as="textarea"
                        rows={10}
                        placeholder="Product description..."
                        name='description'
                        value={description}
                        onChange={handleInputChange}
                    />
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Price</Form.Label>
                    <Form.Control
                        type="number"
                        step=".0001"
                        name='price'
                        value={price}
                        onChange={handleInputChange}
                        placeholder="Price..."
                    />
                </Form.Group>
                <div className="action-buttons-products">
                    <Button variant="primary" type="submit">
                        {titleButtonConfirm}
                    </Button>
                    <Button
                        variant="secondary"
                        onClick={() => history.push("/backoffice/product-list")}
                    >
                        List
                    </Button>
                </div>
            </Form>
            {productId &&
                <ProductImage productId={productId}/>
            }

        </ProductAddEditContainer>
    );
};

export default ProductAddEdit;