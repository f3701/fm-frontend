import styled from 'styled-components';

export const ProductListContainer = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  max-width: 900px;

  .product-table-backoffice tbody{
    position: relative;
  }  
  .wrapper-product-list-title {
    display: flex;
    column-gap: 10px;
    justify-content: space-between;
  }
  .wrapper-product-list-title > *{
    align-self: center;
  }

  .wrapper-product-list-title button {
    font-size: 1.5em;
    padding: 0 10px;
  }
  
  .product-list-actions-panel button {
    margin-left: 5px;
  }

  .loader-product-list {
    height: 100%;
    width: 100%;
  }

  .loader-product-list td{
    padding: 0;
  }

`;

