import React, {useEffect, useState} from 'react';
import {deleteProductAction, fetchProductsPage} from "../../../../services/ProductService";
import {Button, Table} from "react-bootstrap";
import {ProductListContainer} from "./ProductListStyles";
import Paginator from "../../../../components/paginator/Paginator";
import FilterText from "../../../../components/filter-text/FilterText";
import {useHistory} from 'react-router-dom';
import {toastError, toastSuccess} from "../../../../components/alerts/SweetToasts";
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import TextTruncated from "../../../../components/text-truncated/TextTruncated";
import TooltipSwift from "../../../../components/tooltip-swift/TooltipSwift";
import FilterRange from "../../../../components/filter-range/FilterRange";
import {confirmSweet} from "../../../../components/alerts/SweetAlerts";
import LoaderSwift from "../../../../components/loader-swift/LoaderSwift";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faBroom} from "@fortawesome/free-solid-svg-icons";


const ProductList = () => {

    const history = useHistory();

    const defaultFilters = {
        title: '',
        description: '',
        minPrice: '',
        maxPrice: ''
    }
    const [products, setProducts,] = useState([]);
    const [page, setPage,] = useState({});
    const [filters, setFilters,] = useState(defaultFilters);
    const [fetchingPage, setFetchingPage,] = useState(false);

    useEffect(async () => {
        await fetchPage(1);
    }, []);

    const fetchPage = async (pageNumber, filters) => {
        try {
            setFetchingPage(true);
            const results = await fetchProductsPage(pageNumber - 1, filters)
            setProducts(results.content);
            setPage({
                number: results.number,
                totalElements: results.totalElements,
                size: results.size
            });
            setFilters({
                ...filters
            })
        } catch (error) {
            toastError("Could not load products.")
        } finally {
            setFetchingPage(false);
        }
    }

    const filterByTitle = async (title) => {
        await fetchPage(1, {...filters, title});
    }

    const filterByDescription = async (description) => {
        await fetchPage(1, {...filters, description});
    }

    const filterByPrice = async ({minValue: minPrice, maxValue: maxPrice}) => {
        await fetchPage(1, {...filters, minPrice, maxPrice});
    }

    const deleteProduct = async (productId) => {
        if (await confirmSweet(`Are you sure to delete the product?`)) {
            try {
                await deleteProductAction(productId);
                await fetchPage(1);
                toastSuccess('Deleted successfully.');
            } catch (error) {
                toastError(error)
            }
        }
    }

    return (
        <ProductListContainer>

            <div className="wrapper-product-list-title">
                <h1>Products</h1>
                <div className="product-list-actions-panel">
                    <TooltipSwift text="Add product">
                        <Button
                            className="btn-add-product"
                            onClick={() => history.push("/backoffice/create-products")}
                        > + </Button>
                    </TooltipSwift>
                    <TooltipSwift text="Clear filters">
                        <Button
                            className="btn-add-product"
                            onClick={() => fetchPage(1, defaultFilters)}
                        >
                            <FontAwesomeIcon
                                icon={faBroom}
                                size="xs"
                            />
                        </Button>
                    </TooltipSwift>        </div>
            </div>

            <Table striped bordered hover variant="light" className="product-table-backoffice">
                <thead>
                <tr>
                    <th>
                        Title
                        <FilterText
                            submitCallback={filterByTitle}
                            placeholder='Title'
                            defaultValue={filters.title}
                        />
                    </th>
                    <th>
                        Description
                        <FilterText
                            submitCallback={filterByDescription}
                            placeholder='Description'
                            defaultValue={filters.description}
                        />
                    </th>
                    <th>
                        Price
                        <FilterRange
                            submitCallback={filterByPrice}
                            filterName="price"
                            minDefaultValue={filters.minPrice}
                            maxDefaultValue={filters.maxPrice}
                        />
                    </th>
                    <th>
                        Actions
                        <input type="text" className="visibility-hidden display-block"/>
                    </th>
                </tr>
                </thead>

                <tbody>
                <tr className="loader-product-list">
                    <td>{fetchingPage && <LoaderSwift color='rgba(0, 0, 0, 0.25)'/>}
                    </td>
                </tr>
                {products.map(product =>
                    <tr key={product.id}>
                        <td>
                            <TextTruncated
                                text={product.title}
                                lines={1}
                            />
                        </td>
                        <td>
                            <TextTruncated
                                text={product.description}
                                lines={1}
                            />
                        </td>
                        <td>
                            {product.price}
                        </td>
                        <td className="text-align-center">
                            <TooltipSwift text="Edit">
                                <Button
                                    variant="link"
                                    onClick={() => history.push(`/backoffice/edit-products/${product.id}`)}
                                    className="btn-action btn-action-edit"
                                >
                                    <EditIcon/>
                                </Button>
                            </TooltipSwift>
                            <TooltipSwift text="Delete">
                                <Button
                                    variant="link"
                                    onClick={() => deleteProduct(product.id)}
                                    className="btn-action btn-action-delete"
                                >
                                    <DeleteIcon/>
                                </Button>
                            </TooltipSwift>
                        </td>
                    </tr>
                )}
                </tbody>
            </Table>
            {products.length > 0 &&
                <Paginator
                    show={true}
                    page={page}
                    changePageCallback={fetchPage}
                />
            }
        </ProductListContainer>
    );
};

export default ProductList;
