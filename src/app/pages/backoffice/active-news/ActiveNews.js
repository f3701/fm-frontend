import React, {useEffect, useRef, useState} from 'react';
import {ActiveNewsContainer} from "./ActiveNewsStyles";
import axios from "axios";
import {
    archiveNewsImage,
    deleteNewsImage,
    ENDPOINT_NEWS_IMAGE,
    getActiveNewsImages
} from "../../../services/DriveService";
import {toastError, toastSuccess} from "../../../components/alerts/SweetToasts";
import ButtonPrimary from "../../../components/buttons-sweet/button-primary/ButtonPrimary";
import ButtonDanger from "../../../components/buttons-sweet/button-danger/ButtonDanger";
import ButtonAlternative from "../../../components/buttons-sweet/button-alternative/ButtonAlternative";
import {confirmSweet} from "../../../components/alerts/SweetAlerts";

const ActiveNews = () => {

    const [images, setImages] = useState([]);
    const MAX_HEIGHT_IMAGES = 200;

    const [currentFile, setCurrentFile] = useState('');
    const [currentFileContent, setCurrentFileContent] = useState('');
    const fileInputRef = useRef();

    const handleInputFileChange = (event) => {
        event.target.files[0] && setCurrentFile(URL.createObjectURL(event.target.files[0]));
        event.target.files[0] && setCurrentFileContent(event.target.files[0]);
    }

    const handleConfirmUpload = async () => {
        await uploadFile();

        setCurrentFile('');
        fileInputRef.current.value = '';
        toastSuccess('Uploaded.')
    }

    const handleCancelUpload = () => {
        setCurrentFile('');
        fileInputRef.current.value = '';
    }

    useEffect(async () => {
        setImages(await fetchActiveImages())
    }, []);

    const uploadFile = async () => {
        const formData = new FormData();
        formData.append('file', currentFileContent);
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        await axios.post(ENDPOINT_NEWS_IMAGE, formData, config)
            .catch(error => {
                toastError(error?.response?.data?.data.message);
                throw error;
            });

        setImages(await fetchActiveImages());
    }

    const handleDeleteFile = async (id) => {

        if (await confirmSweet('Are you sure to DELETE this item?')) {
            await deleteNewsImage(id);
            setImages(await fetchActiveImages());
            toastSuccess('Deleted.');
        }
    }

    const handleArchiveFile = async (id) => {
        await archiveNewsImage(id);
        setImages(await fetchActiveImages());
        toastSuccess('Archived.');
    }

    const fetchActiveImages = async () => {
        const images = (await getActiveNewsImages()).reverse();
        return images.map((image) => ({
            id: image.id,
            url: ENDPOINT_NEWS_IMAGE + `/${image.id}/file`
        }));
    }

    return (
        <ActiveNewsContainer>

            <h1>Active News</h1>

            <div className="file-upload-cont">
                <input
                    type="file"
                    name="inputUploadImage"
                    onChange={handleInputFileChange}
                    ref={fileInputRef}
                />
            </div>

            {
                currentFile && (
                    <div className='preview-cont'>
                        <div className="preview-image-cont">
                            <img src={currentFile} alt='preview image'/>
                        </div>
                        <ButtonPrimary
                            title="Upload"
                            onClick={handleConfirmUpload}
                        />
                        <ButtonAlternative
                            title="Cancel"
                            onClick={handleCancelUpload}
                        />
                    </div>
                )
            }
            <br/>
            <div className="list-images-cont">
                {
                    images.map(i => (
                        <div key={i.id}>
                            <img src={i.url} alt="img" height={MAX_HEIGHT_IMAGES}/>
                            <div>
                                <ButtonDanger
                                    title="Delete"
                                    onClick={() => handleDeleteFile(i.id)}
                                />
                                <ButtonPrimary
                                    title="Archive"
                                    onClick={() => handleArchiveFile(i.id)}
                                />
                            </div>
                        </div>
                    ))
                }
            </div>

        </ActiveNewsContainer>
    );
};

export default ActiveNews;
