import React, {useEffect, useState} from 'react';
import {ArchivedNewsContainer} from "./ArchivedNewsStyles";
import {
    deleteNewsImage,
    enableNewsImage,
    ENDPOINT_NEWS_IMAGE,
    getArchivedNewsImages
} from "../../../services/DriveService";
import {toastSuccess} from "../../../components/alerts/SweetToasts";
import ButtonDanger from "../../../components/buttons-sweet/button-danger/ButtonDanger";
import ButtonPrimary from "../../../components/buttons-sweet/button-primary/ButtonPrimary";
import {confirmSweet} from "../../../components/alerts/SweetAlerts";

const ArchivedNews = () => {

    const [images, setImages] = useState([]);
    const MAX_HEIGHT_IMAGES = 200;

    useEffect(async () => {
        setImages(await fetchArchivedImages())
    }, []);

    const fetchArchivedImages = async () => {
        const images = await getArchivedNewsImages();
        return images.map((image) => ({
            id: image.id,
            url: ENDPOINT_NEWS_IMAGE + `/${image.id}/file`
        }));
    }

    const handleDeleteFile = async (id) => {
        if (await confirmSweet('Are you sure to DELETE this item?')) {
            await deleteNewsImage(id);
            setImages(await fetchArchivedImages());
            toastSuccess('Deleted.');
        }
    }

    const handleEnableFile = async (id) => {
        await enableNewsImage(id);
        setImages(await fetchArchivedImages());
        toastSuccess('Enabled.');
    }

    return (
        <ArchivedNewsContainer>

            <h1>Archived News</h1>

            <div className="list-images-cont">
                {
                    images.map(i => (
                        <div key={i.id}>
                            <img src={i.url} alt="img" height={MAX_HEIGHT_IMAGES}/>
                            <div>
                                <ButtonDanger
                                    title="Delete"
                                    onClick={() => handleDeleteFile(i.id)}
                                />
                                <ButtonPrimary
                                    title="Enable"
                                    onClick={() => handleEnableFile(i.id)}
                                />
                            </div>

                        </div>
                    ))
                }
            </div>

        </ArchivedNewsContainer>
    );
};

export default ArchivedNews;
