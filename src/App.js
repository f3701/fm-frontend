import './App.css';
import RouterMain from './app/router/RouterMain';
import {Toaster} from "react-hot-toast";

function App() {
  return (
      <>
        <RouterMain />
        <Toaster />
      </>
  );
}

export default App;

